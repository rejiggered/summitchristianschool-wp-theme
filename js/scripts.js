(function ($, root, undefined) {

	$(function () {

		'use strict';

		var url = scriptPath.template_url;



$(window).scroll(function() {
if ($(this).scrollTop() > 20){
	$('header').addClass("sticky");
  }
  else{
	$('header').removeClass("sticky");
  }
});





//https://github.com/nicolafranchini/VenoBox
 $('.modal-trigger').venobox({
	closeBackground: '#62b2df',
	closeColor: '#fff',
	overlayColor : 'rgba(67,66,66,0.90)',
	titleBackground: '#f3efe0',
	titleColor: '#434242',
	framewidth: '600px',
	frameheight: 'auto'
 });

 $('.modal-video').venobox({
	closeBackground: '#62b2df',
	closeColor: '#fff',
	overlayColor : 'rgba(67,66,66,0.90)',
	titleBackground: '#f3efe0',
	titleColor: '#434242'
 });




 $('.gallery').venobox({
	closeBackground: '#62b2df',
	closeColor: '#fff',
	overlayColor : 'rgba(67,66,66,0.90)',
	titleBackground: '#f3efe0',
	titleColor: '#434242',
	numeratio: true,
	numerationBackground: '#434242'
 });








//https://github.com/michalsnik/aos
AOS.init({
      duration: 800,
      easing: 'ease-out-sine',
      once: true,
      offset: -200
    });


// Homepage banner crossfade https://github.com/terwanerik/FadeShow
$(function(){
	var url = scriptPath.template_url;;
	$(".banner.home").fadeShow({
	images: [ url + '/img/banner-home1.jpg', url + '/img/banner-home2.jpg', url + '/img/banner-home3.jpg'],
	correctRatio: true
	});
});





$("#menu").mmenu({
	onClick: {
		close: false
	 },
	 offCanvas: {
		pageSelector: "#container",
		position  : "right",
		zposition : "front"
	 },
	 extensions: ["pagedim", "border-none"],
	 navbar: {
		"add": false
	 },
	 slidingSubmenus: false,
	 transitionDuration: 100
  });


var API = $("#menu").data( "mmenu" );
   $("#menu-open").click(function() {
	 API.open();
  });
  $("#menu-close").click(function() {
	 API.close();
  });
  $("#menu li a").click(function() {
	 API.close();
 });





$(function() {
	$('.quicklink a').matchHeight();
});




$('.testimonials--wrapper').cycle({
    slides: 'blockquote',
    prev: '#slidePrev',
    next: '#slideNext',
    swipe: true,
	speed: 800,
	timeout: 8000,
	pauseOnHover: true,
	autoHeight: 'container',
	updateView: 1
});



// http://jquery.eisbehr.de/lazy/
 $(function() {
        $('.lazy').Lazy({
         effect: "fadeIn",
          effectTime: 700,
          threshold: 0
        });
});



svg4everybody();




	});

})(jQuery, this);
