<?php /* Template Name: Admissions - Preschool Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">Tuition / Fee Schedule</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Below you'll find the tuition/fee schedule for the <?php echo get_field('school-year', 'option'); ?> school year. To provide for the different needs of our students and their families, we offer several options for each age group/class, as well as a discount for those students with a sibling attending Summit Christian School.</p>
				</div>
			</div>


			<section class="tuitionTable">
				<h3 class="tuitionTable--heading h4">Preschool (<?php echo get_field('school-year', 'option'); ?>)</h3>
				<div class="table-responsive">
				<?php
				$table = get_field( 'tuition-table' );
					if ( $table ) {
						echo '<table class="table">';
							if ( $table['header'] ) {
								echo '<thead>';
									echo '<tr>';
										foreach ( $table['header'] as $th ) {

											echo '<th>';
												echo $th['c'];
											echo '</th>';
										}
									echo '</tr>';
								echo '</thead>';
							}
							echo '<tbody>';
								foreach ( $table['body'] as $tr ) {
									echo '<tr>';
										foreach ( $tr as $td ) {
											echo '<td>';
												echo $td['c'];
											echo '</td>';
										}
									echo '</tr>';
								}
							echo '</tbody>';
						echo '</table>';
					}
					?>
				</div>
				<img class="swipe visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/swipe.svg" />
			</section>



			<section class="row">
				<div class="col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-0">
					<h5 class="well--heading icon"><svg class="icon-doc"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-doc"></use></svg>Download Application Forms</h5>
					<p>Download and complete the preschool application form and return to the school office.</p>
					<div class="well col-sm-7 col-sm-offset-0 col-md-12">
						<a href="<?php $file = get_field('preschool-application', 'option'); echo $file['url']; ?>" class="btn btn-default btn-block">Preschool Application</a>
					</div>
				</div>

				<div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-1">

				<?php
				// check if the repeater field has rows of data
				if( have_rows('bulleted-list') ):

					// loop through the rows of data
					while ( have_rows('bulleted-list') ) : the_row(); ?>

						<h4><?php the_sub_field('list-heading'); ?></h4>
						<ul>
						<?php // loop through the rows of data

						while ( have_rows('list-items') ) : the_row(); ?>

						<li><?php the_sub_field('list-item');?></li>

						<?php endwhile; ?>

						</ul>

					<?php endwhile;
				else :
				endif;
				?>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
