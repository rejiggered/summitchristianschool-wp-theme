<ul class="row">
<?php

	$staff_query = get_staff();

	while( $staff_query->have_posts() ) {
		$staff_query->the_post();

		$staff_department = get_field('staff-department');
		$bio = get_field('staff-bio'); ?>

		<?php if( $staff_department == $departmentID && $bio ) { ?>

			<li class="staff--person col-sm-6 col-md-4">
				<a href="#<?php global $post; echo $post->post_name; ?>" class="modal-trigger" data-vbtype="inline" title="<?php the_title(); ?>">
					<div class="staff--person-overlay"></div>
					<div class="staff--person-details">
						<h4 class="staff--person-name"><?php the_title(); ?></h4>
						<p class="staff--person-title"><?php the_field('staff-title'); ?></p>
					</div>
				<?php
					$image = get_field('staff-photo');

					if( $image ) { ?>
							<img class="lazy" data-src="<?php the_field('staff-photo'); ?>" alt="<?php the_title(); ?>" />
					<?php }
					else { ?>
							<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/person-placeholder.png" ?>
					<?php }
				?>
				</a>

				<!--  MODAL -->
				<div id="<?php global $post; echo $post->post_name; ?>" class="modal">

				<?php
					if( $image ) { ?>
					<div class="rounded">
						<img src="<?php the_field('staff-photo'); ?>" class="rounded" alt="<?php the_title(); ?>" />
					</div>
					<?php }
				?>

					<h4 class="h3 staff--person-name"><?php the_title(); ?></h4>
					<p class="staff--person-title"><?php the_field('staff-title'); ?></p>
					<div class="staff--person-bio">
						<?php the_field('staff-bio'); ?>
					</div>
				</div>
			</li>

		<?php }

		elseif( $staff_department == $departmentID ) { ?>

			<li class="staff--person col-sm-6 col-md-4">
				<a title="<?php the_title(); ?>">
					<div class="staff--person-overlay"></div>
					<div class="staff--person-details">
						<h4 class="staff--person-name"><?php the_title(); ?></h4>
						<p class="staff--person-title"><?php the_field('staff-title'); ?></p>
					</div>
				<?php
					$image = get_field('staff-photo');

					if( $image ) { ?>
							<img class="lazy" data-src="<?php the_field('staff-photo'); ?>" alt="<?php the_title(); ?>" />
					<?php }
					else { ?>
							<img class="lazy" data-src="<?php echo get_template_directory_uri(); ?>/img/person-placeholder.png" ?>
					<?php }
				?>
				</a>
			</li>
	<?php }

	}


	wp_reset_postdata();
?>
</ul>

