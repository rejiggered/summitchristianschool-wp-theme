<?php

	$testimonial_query = get_testimonials();

	while( $testimonial_query->have_posts() ) {
	$testimonial_query->the_post(); ?>

	<blockquote data-slidr="<?php the_title(); ?>">
		<p>“<?php the_field('testimonial'); ?>”</p>
		<cite><?php the_title(); ?></cite>
	</blockquote>

<?php  } wp_reset_postdata(); ?>

