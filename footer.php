			<!-- footer -->
			<footer class="footer" role="contentinfo">

				<div class="footer--container container">
					<div class="row">
						<p class="copyright col-xs-8">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
						<span class="col-xs-4 footer--social"><a href="https://www.facebook.com/summitcs"><svg class="icon-facebook"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-facebook"></use></svg></a></span>
					</div>
					<p class="legal">Summit Christian School admits students of any race, color, national and ethnic origin to all the rights, privileges, programs, and activities generally accorded or made available to students at the school. It does not discriminate on the basis of race, color, national and ethnic origin in administration of its educational policies, admissions policies, scholarship and loan programs, and athletic and other school-administered programs.</p>
				</div>


			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>




	</body>
</html>
