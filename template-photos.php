<?php /* Template Name: Temp Photos Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">


            <h4 class="margin-bottom20 col-sm-12">Extra photos</h4>
			<div class="margin-bottom30 clearfix">
			<?php
				$x = 37;
				$photo = 37;
				while($x <= 41) { ?>

					<a class="gallery gallery-samples col-sm-3" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/candidates/candidate<?php echo $photo;?>.jpg" title="#<?php echo $photo;?>">
						<p>Photo #<?php echo $photo;?></p>
						<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/candidates/candidate<?php echo $photo;?>.jpg" class="lazy" alt="<?php echo $photo;?>">
					</a>

				<?php
					$x++;
					$photo++;
				}
				?>
			</div>




			<h4 class="margin-bottom20 col-sm-12">Summit photos</h4>
			<div class="margin-bottom30 clearfix">
			<?php
				$x = 1;
				$photo = 1;
				while($x <= 36) { ?>

					<a class="gallery gallery-samples col-sm-3" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/candidates/candidate<?php echo $photo;?>.jpg" title="#<?php echo $photo;?>">
						<p>Photo #<?php echo $photo;?></p>
						<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/candidates/candidate<?php echo $photo;?>.jpg" class="lazy" alt="<?php echo $photo;?>">
					</a>

				<?php
					$x++;
					$photo++;
				}
				?>
			</div>





			<h4 class="margin-bottom20 col-sm-12 ">Preschool photos</h4>
			<div class="">
			<?php
				$x = 1;
				$photo = 1;
				while($x <= 8) { ?>

					<a class="gallery gallery-samples col-sm-3" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-<?php echo $photo;?>.jpg" title="#<?php echo $photo;?>">
						<p>Photo #<?php echo $photo;?></p>
						<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-<?php echo $photo;?>.jpg" class="lazy" alt="Preschool photo <?php echo $photo;?>">

					</a>

				<?php
					$x++;
					$photo++;
				}
				?>
			</div>



		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
