<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">We can't seem to find the page you're looking for.</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>The page you were looking for appears to have been moved, deleted or does not exist. Head <a href="/">back to our home page</a> and give it another try.</p>
				</div>
			</div>





		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
