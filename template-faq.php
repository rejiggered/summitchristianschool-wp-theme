<?php /* Template Name: FAQ Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">Get your questions <em>answered</em></h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Answers to some of the most frequently asked questions we receive can be found below. Don’t see <em>your</em> question? <a href="<?php echo get_permalink(294); ?>">Call our office</a> for more information! We'd love to hear from you.</p>
				</div>
			</div>


			<section class="row">

				<div class="faqs">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<div class="col-sm-8 col-sm-push-4 col-md-7 col-md-push-5">
						<dl>
					<?php

						$post_type = 'faqs';

						$args=array(
							'post_type' => $post_type,
							'order' => 'ASC',
						);
						$my_query = null;
						$my_query = new WP_Query($args);
						if( $my_query->have_posts() ) { ?>

							<?php

							while ($my_query->have_posts()) : $my_query->the_post();?>


							<dt><?php the_title(); ?></dt>
							<dd><?php the_field('faq-answer'); ?></dd>

							<?php endwhile; // End of the loop.

						}

						wp_reset_query();  ?>
						</dl>
					</div>


					<?php endwhile; ?>

				<?php endif; ?>
				</div>


				<aside class="col-sm-8 col-sm-push-4 col-md-4 col-md-push-0 col-md-pull-7">
					<h5 class="well--heading">Still have questions?</h5>
					<p>We invite you to contact the school office. We'd love to hear from you and answer any additional questions you may have.</p>
					<div class="well col-sm-7 col-sm-offset-0 col-md-12">
						<a href="<?php echo get_permalink(294); ?>" class="btn btn-default btn-block ">Contact Our Office</a>
					</div>
				</aside>

			</section>

		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
