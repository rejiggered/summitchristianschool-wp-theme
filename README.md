This is the custom, responsive Wordpress theme (based on HTML 5 Blank and Bootstrap 3.x) I developed for Summit Christian School. It makes heavy use of Advanced Custom Fields and custom post types.

http://www.summitchristianschool.com/