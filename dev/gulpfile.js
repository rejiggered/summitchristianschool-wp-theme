var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var include = require('gulp-include');
var uglify = require('gulp-uglify');

var input = 'scss/**/*.scss';
var output = '../';


gulp.task('sass', function () {
	return gulp.src(input)
	.pipe(sass({ outputStyle: 'compressed' }))
	.on('error', sass.logError)
	.pipe(autoprefixer({ browsers: ['last 2 versions', '> 5%', 'Firefox ESR'] }))
	.pipe(gulp.dest(output))
	.pipe(browserSync.stream());
});


// Concat plugins
gulp.task('js', function() {
    gulp.src(['js/plugins.js'])
        .pipe( include() )
        .pipe( uglify()	)
        .pipe( gulp.dest("../js") )
});



// Watch files for change and set Browser Sync
gulp.task('watch', function() {
	// BrowserSync settings
	browserSync.init({
	proxy: "dev.summitchristianschool.com",
	files: "style.css",
	notify: false,
	browser: "firefox"
//	server: true
});





// Scss file watcher
gulp.watch(input, ['sass'])
gulp.watch('*.html').on('change', browserSync.reload);
gulp.watch(('js/custom.js')).on('change', browserSync.reload);
gulp.watch(('dev/js/plugins.js')).on('change', browserSync.reload);
});



// Default task
gulp.task('default', ['sass', 'watch', 'js']);
