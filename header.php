<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link rel="dns-prefetch" href="//fonts.googleapis.com">
		<link rel="dns-prefetch" href="//www.google-analytics.com">

        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link href='//fonts.googleapis.com/css?family=Libre+Franklin:300,300i,400,400i,600|Merriweather:400,400i,700' rel='stylesheet' type='text/css' />

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<!-- Main Nav -->
		<nav id="menu">
			<button type="button" class="btn-link" id="menu-close"><svg class="icon-close"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-close"></use></svg></button>
		<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'main-menu',
					'menu'            => 'Menu',
					'container'       => '',
					'container_id'    => '',
					'container_class' => '',
					'menu_class'      => 'mm-nolistview',
					'menu_id'         => '',
					'echo'            => true,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul class="mm-nolistview">%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
					)
				);
		?>
		</nav>


		<!-- wrapper -->
		<div id="container">


			<!-- header -->
			<header class="header">
				<div class="header--wrapper">
					<a href="<?php echo home_url(); ?>" class="logo">
						<?php include 'includes/summit-logo.php';?>
					</a>
					<button type="button" class="btn-link" id="menu-open"><span>Menu</span><svg class="icon-menu"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-menu"></use></svg></button>
				</div>
			</header>
			<!-- /header -->

		<?php
			if( is_front_page() ) { ?>

			<section class="banner home">
				<div class="banner--text container">
					<div class="row">
						<div class="col-lg-10 col-lg-offset-1">
							<h1 class="banner--heading" data-aos="fade-down" data-aos-delay="600">Summit Christian School</h1>
							<p class="banner--subheading" data-aos="fade-up" data-aos-delay="800">A Christ-Focused Christian School in Fair Oaks, CA</p>
							<div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4" data-aos="fade" data-aos-delay="1600">
								<a href="<?php echo get_permalink(6); ?>" class="btn btn-transparent btn-block banner--btn">Learn About Us</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		<?php }

			elseif( is_404() ) { ?>

			<section class="banner">
				<div class="banner--text container">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<h1 class="banner--heading" data-aos="fade-down" data-aos-delay="600">Oops!</h1>
						</div>
					</div>
				</div>
			</section>

		<?php }


			else { ?>

			<section class="banner lazy-banner">
				<div class="banner-inner"></div>
				<div class="banner--text">
				<?php if($post->post_parent) {
					$parent_title = get_the_title($post->post_parent); ?>
					<h2 class="banner--subheading" data-aos="fade-down" data-aos-delay="900"><a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parent_title ?></a></h2>
					<h1 class="banner--heading" data-aos="fade-up" data-aos-delay="600"><?php the_title(); ?></h1>

     			<?php } else { ?>
     				<h1 class="banner--heading" data-aos="fade-up" data-aos-delay="600"><?php the_title(); ?></h1>
     			<?php } ?>
     			</div>


				<?php
					$children = get_pages('child_of='.$post->ID);

					//GET PARENT PAGE IF THERE IS ONE
					$parent = $post->post_parent;

					//DO WE HAVE SIBLINGS?
					$siblings =  get_pages('child_of='.$parent);

				if( count($children) != 0) {
					   $args = array(
						 'depth' => 0,
						 'title_li' => '',
						 'child_of' => $post->ID
					   );

					} elseif($parent != 0) {
						$args = array(
							 'depth' => 0,
							 'title_li' => '',
							 'child_of' => $parent
						   );
					}
					//Show pages if this page has more than one sibling
					// and if it has children
					if(count($siblings) > 1 && !is_null($args))
					{?>
						 <ul class="subnav">
						 <?php wp_list_pages($args);  ?>
						 </ul>
				<?php }  ?>
			</section>

		<?php  } ?>