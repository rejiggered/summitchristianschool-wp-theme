<?php /* Template Name: Admissions Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">An overview of our admission process</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Thank you for considering Summit Christian School for the upcoming <?php echo get_field('school-year', 'option'); ?> school year. We consider it a privilege to partner with you in educating your children, and look forward to working with you throughout the application process.</p>
				</div>
			</div>


			<section class="row">
				<div class="col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-0">
					<h5 class="well--heading icon"><svg class="icon-doc"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-doc"></use></svg>Application Forms</h5>
					<p>Download and complete the student application form(s) and return to the school office.</p>
					<div class="well col-sm-7 col-sm-offset-0 col-md-12">
						<a href="<?php $file = get_field('new-student-application', 'option'); echo $file['url']; ?>" class="btn btn-default btn-block ">New Student Application</a>
						<a href="<?php $file = get_field('returning-student-application', 'option'); echo $file['url']; ?>" class="btn btn-secondary btn-block">Returning Student Application</a>
						<a href="<?php $file = get_field('new-student-application', 'option'); echo $file['url']; ?>" class="btn btn-secondary btn-block">Preschool Application</a>
					</div>
				</div>



				<div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-1">

				<?php
				// check if the repeater field has rows of data
				if( have_rows('bulleted-list') ):

					// loop through the rows of data
					while ( have_rows('bulleted-list') ) : the_row(); ?>

						<h4><?php the_sub_field('list-heading'); ?></h4>
						<ul>
						<?php // loop through the rows of data

						while ( have_rows('list-items') ) : the_row(); ?>

						<li><?php the_sub_field('list-item');?></li>

						<?php endwhile; ?>

						</ul>

					<?php endwhile;
				else :
				endif;
				?>


					<p class="small"><em>* For those seeking tuition assistance, a $100 non-refundable deposit must be made toward the registration fee, with the remaining balance to be paid when and if tuition assistance award is accepted.</em>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
