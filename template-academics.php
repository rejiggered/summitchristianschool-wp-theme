<?php /* Template Name: Academics Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">Our students regularly score in the <em>top 20% nationally</em></h2>
				<div class="lead col-sm-8 col-md-7">
				<?php the_field('page-intro'); ?>
				</div>
			</div>

			<section class="row">
				<div class="col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-0">
					<h5 class="well--heading icon"><svg class="icon-bar-chart"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-bar-chart"></use></svg>View National Testing Scores</h5>
					<p>Download and view our latest TerraNova standardized testing results.</p>
					<div class="well col-sm-7 col-sm-offset-0 col-md-12">
						<a href="<?php $file = get_field('terra-nova', 'option'); echo $file['url']; ?>" class="btn btn-default btn-block">TerraNova Testing Results</a>
					</div>
				</div>
				<div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-1">
					<h3>Curriculum</h3>
					<p>All phases of the curriculum are directed to assist in the ultimate development of the whole child. Emphasis is placed upon student mastery of academic fundamentals and spiritual development.</p>
					<p>To accomplish that, Summit Christian School uses <a href="http://www.bjupress.com/">Bob Jones University Press (BJUP)</a> textbooks in all grades, from our preschool classes through 8th grade. The textbooks are written from the standpoint that the Bible is the ultimate source of authority. Not only do the textbooks meet the requirements of state standards and conventions, they also incorporate scripture references, questions that provoke Biblical thought and side-bars (appropriate to the age of the students) that connect the content information with a Biblical worldview. Our goal is that students learn to see every part of life through the lens of scripture.</p>

					<h4>National Testing Scores</h4>
					<p><a href="http://www.setontesting.com/terranova/">TerraNova standardized testing</a> is administered every Spring. The objective for Summit in testing is to be able to assess that we are maintaining our standards and exceeding state standards. Our students regularly score in the top 20% nationally.</p>

					<h4>Common Core State Standards (CCSS)</h4>
					<p>As a private Christian school, Summit is not required to follow Common Core. Our philosophy about CCSS follows. A “standard” is a written statement that lists a skill or knowledge-set that a student should master by a certain point in their schooling. Where those kinds of standards are written, our curriculum meets, or in many cases, exceeds them. Should certain standards try to dictate what we should teach or how we should teach it, and if those standards conflict with our own beliefs or curriculum, we simply disregard them. Common Core does not impact Summit in any negative respect.</p>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
