<?php /* Template Name: Staff Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container staff">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">Our staff is <em>real</em>: real with their students, with parents and with one another</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Like all schools, the teachers make it what it is, and that's what makes Summit so special! Summit's teachers are not just experienced and highly effective, they are also loving, God-seeking, Christ-pointing, joyful living, prayerful and full of grace. Our staff is real: real with their students, with parents and with one another. They exemplify healthy relationships and community to our families.</p>
					<p>But that's only half the team that makes Summit so special. The office and support staff work out of the joy in their hearts and numerous volunteers donate countless hours out of appreciation for all God is doing through Summit. Below is a bit of info on each familiar face you may see serving the Lord through Summit students.</p>
				</div>
			</div>


			<div class="staff--dept">
				<h3 class="staff--dept-heading">Administration</h3>
				<?php
					$departmentID = "administration";
					include("includes/get-staff.php");
				?>
			</div>

			<div class="staff--dept">
				<h3 class="staff--dept-heading">Elementary</h3>
				<?php
					$departmentID = "elementary";
					include("includes/get-staff.php");
				?>
			</div>

			<div class="staff--dept">
				<h3 class="staff--dept-heading">Middle School</h3>
				<?php
					$departmentID = "middle";
					include("includes/get-staff.php");
				?>
			</div>

			<div class="staff--dept">
				<h3 class="staff--dept-heading">Preschool</h3>
				<?php
					$departmentID = "preschool";
					include("includes/get-staff.php");
				?>
			</div>

			<div class="staff--dept">
				<h3 class="staff--dept-heading">Support Staff & Aides</h3>
				<?php
					$departmentID = "support";
					include("includes/get-staff.php");
				?>
			</div>

			<div class="staff--dept row">
				<div class="col-sm-7 col-md-7">
				<h4 class="staff--dept-heading">Board of Directors</h4>
					<p>Summit Christian School's Board of Directors is a professional board that provides Godly leadership, oversight and policy direction to the school and its Administrator, and is composed mainly of parents. The board members bring diverse skill sets to the governance of the school, gained from professions in business management, accounting and entrepreneurship. The Board meets monthly and all board meetings are open to the Summit Community.</p>
				</div>
				<div class="col-sm-5 col-md-4 col-md-offset-1">
					<h4 class="staff--dept-heading">Board Members</h4>
 				<?php
				// check if the repeater field has rows of data
				if( have_rows('board-members') ): ?>
					<ul>
					<?php while ( have_rows('board-members') ) : the_row();

						$member = get_sub_field('board-member');
						$memberRole = get_sub_field('board-member-role'); ?>

						<?php if( $memberRole ): ?>
							<li><?php echo $member; ?>, <?php echo $memberRole; ?></li>
						<?php endif; ?>

						<?php if( ! $memberRole ): ?>
							<li><?php echo $member; ?></li>
						<?php endif; ?>

					<?php endwhile;
				else :
				endif;
				?>
					</ul>

				</div>
			</div>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
