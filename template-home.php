<?php /* Template Name: Homepage Template */ get_header(); ?>


	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<section class="notification">
			<div class="container">

		<?php

		$checkbox = get_field('notification-choice');
		$endDate = strtotime(get_field('notification-end-date'));
		$today = strtotime('now');

		if( $endDate > $today && $checkbox ){

			$post_object = get_field('notification');

			if( $post_object ):

				// override $post
				$post = $post_object;
				setup_postdata( $post );

				if(get_field('notification-additional-choice')) { ?>

				<div class="row">
					<div class="col-sm-9 col-lg-10">
						<h3 class="notification--title"><?php the_title(); ?></h3>
						<p class="notification--description"><?php the_field('notification-text'); ?></p>
					</div>
					<div class="col-xs-8 col-xs-offset-2 col-sm-3 col-sm-offset-0 col-lg-2 notification--btn">
						<a href="#notification-more" class="btn btn-default btn-sm btn-block modal-trigger" data-vbtype="inline" title="<?php the_title(); ?>">Learn More</a>
					</div>
				</div>
				<div class="modal" id="notification-more">
					<h4 class="notification--title"><?php the_title(); ?></h4>
					<p><?php the_field('notification-additional-text'); ?>
				</div>

				<?php
				}

				elseif( ! get_field('notification-additional-choice')) { ?>
				<h3 class="notification--title"><?php the_title(); ?></h3>
				<p class="notification--description"><?php the_field('notification-text'); ?></p>

				<?php }
			endif;

			wp_reset_postdata();
		}
		else { ?>
			<h3 class="notification--title">Have questions about Summit?</h3>
			<p class="notification--description">What’s the best way to learn about Summit Christian School? Spend the day here! We offer “shadow days” for your child to experience a day in the life of a Summit student. <a href="<?php echo get_permalink(294); ?>">Contact us to schedule a tour</a>.</p>
			<?php
				update_field('notification-choice', '');
				update_field('notification', '');
				update_field('notification-end-date', '');
		}
		?>
			</div>
		</section>

		<main role="main">
			<div class="container">
				<section class="overview section">
					<div class="row">
					<div class="col-sm-8 col-sm-offset-2 video--thumb">
					    <a class="modal-video" data-vbtype="vimeo" data-autoplay="true" href="https://vimeo.com/88419699" title="Summit Christian School Video">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/video-thumb.jpg" alt="" />
					    </a>
					    <svg class="icon-play"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-play"></use></svg>
					</div>
					</div>

					<div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            <h2 class="section--title ">Learning and Living with Christian Character</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="lead col-lg-10 col-lg-offset-1">
                            <p>We are an independent, inter-denominational school with classes from preschool to 8th grade, located on the grounds of Grace Bible Church in Fair Oaks, California. We are members of the <a href="https://www.acsi.org/">Association of Christian Schools International</a> and the <a href="https://www.capso.org/">California Association of Private School Organizations</a>.
                        </div>
                        <div class="col-lg-10 col-lg-offset-1">
                            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4">
                                <a class="modal-video btn btn-default btn-block" data-vbtype="vimeo" data-autoplay="true" href="https://vimeo.com/88419699" title="Summit Christian School Video">Watch Our Video Overview</a>
                            </div>
                        </div>
                    </div>
				</section>


				<nav class="quicklinks section">
					<ul class="row">
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(294); ?>">
								<div class="quicklink--icon">
									<svg class="icon-map-marker"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-map-marker"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">Contact & Directions</h3>
									<p class="quicklink--description">Need more information? Contact us for more.</p>
								</div>
							</a>
						</li>
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(261); ?>">
								<div class="quicklink--icon">
									<svg class="icon-graduation"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-graduation"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">Tuition & Fees</h3>
									<p class="quicklink--description">Our competitive tuition combined with outstanding faculty and programs make us one of the best educational values in the Sacramento Area.</p>
								</div>
							</a>
						</li>
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(13); ?>">
								<div class="quicklink--icon">
									<svg class="icon-doc"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-doc"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">Application Forms</h3>
									<p class="quicklink--description">Take the first step toward joining the Summit family.</p>
								</div>
							</a>
						</li>
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(431); ?>#tools">
								<div class="quicklink--icon">
									<svg class="icon-laptop"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-laptop"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">Online Tools</h3>
									<p class="quicklink--description">Parents, stay connected. View homework assignments and make tuition payments online.</p>
								</div>
							</a>
						</li>
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(297); ?>">
								<div class="quicklink--icon">
									<svg class="icon-gift"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-gift"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">Support Summit</h3>
									<p class="quicklink--description">Make a difference in the lives of our students with a tax-deductible donation.</p>
								</div>
							</a>
						</li>
						<li class="quicklink col-sm-6 col-md-4">
							<a href="<?php echo get_permalink(162); ?>">
								<div class="quicklink--icon">
									<svg class="icon-bar-chart"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-bar-chart"></use></svg>
								</div>
								<div class="quicklink--text">
									<h3 class="quicklink--title">National Ranking</h3>
									<p class="quicklink--description">Our students excel far above most other school systems in the area and our scores prove it.</p>
								</div>
							</a>
						</li>
					</ul>
				</nav>


			    <figure class="row">
                    <div class="imageGrid col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0 ">
                        <div class="image-container">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-1.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-1.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-1.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-2.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-2.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-2.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container third">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-3.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-3.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-3.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container third">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-4.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-4.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-4.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container third">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-5.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-5.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-5.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-6.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-6.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-6.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                        <div class="image-container">
                            <div class="image">
                                <a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/home-7.jpg" title="Summit Christian School Photos">
                                    <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-7.jpg" class="lazy" alt="Summit Christian School Photos">
                                </a>
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/home-7.jpg" class="lazy visible-xs" alt="Summit Christian School Photos">
                            </div>
                        </div>
                    </div>
			    </figure>


				<section class="testimonials row section">
					<h2 class="section--title">What Parents Are Saying</h2>
					<div class="testimonials--wrapper col-xs-10 col-xs-offset-1">
					<?php
						include("includes/get-testimonials.php");
					?>
					</div>
						<svg id="slidePrev" class="icon-chevron-left"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-chevron-left"></use></svg>
						<svg id="slideNext" class="icon-chevron-right"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-chevron-right"></use></svg>
				</section>


		<?php endwhile; ?>


		<?php endif; ?>

			</div>
			<!-- /section -->
		</main>


<?php get_footer(); ?>
