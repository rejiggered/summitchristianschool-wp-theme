<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 1170;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support


    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/



// =========================================================================
// ADD DESIRED IMAGE SIZES
// =========================================================================

// add_image_size( 'staff-small', 400, 400, true );
// add_image_size( 'staff-medium', 800, 800, true);
//
//
// add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
// function wpshout_custom_sizes( $sizes ) {
//     return array_merge( $sizes, array(
//         'staff-small' => __( 'Staff Small' ),
//         'staff-medium' => __( 'Staff Medium' ),
//     ) );
// }



// =========================================================================
// REMOVE UNWANTED IMAGE SIZES
// =========================================================================

function paulund_remove_default_image_sizes( $sizes) {
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['medium_large']);

    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'paulund_remove_default_image_sizes');


//add_filter('jpeg_quality', function($arg){return 50;});






// Custom scripting to move JavaScript from the head to the footer
function remove_head_scripts()
{
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'remove_head_scripts');


// Remove WP embed script
function speed_stop_loading_wp_embed() {
if (!is_admin()) {
wp_deregister_script('wp-embed');
}
}
add_action('init', 'speed_stop_loading_wp_embed');


// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

// Disable oEmbed Discovery Links
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);





// Load HTML5 Blank scripts (header.php)
function html5blank_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('summitplugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0.0', yes); // Plugins
        wp_enqueue_script('summitplugins'); // Enqueue it!

        wp_register_script('summitscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', yes); // Custom scripts
        wp_enqueue_script('summitscripts'); // Enqueue it!


    	wp_localize_script( 'summitscripts', 'scriptPath', array( 'template_url' => get_bloginfo('template_url') ) );

    }
}




// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load HTML5 Blank styles
function html5blank_styles()
{

    wp_register_style('summitchristian', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('summitchristian'); // Enqueue it!
}

// Register Summit Navigation
function summit_main_menu() {
  register_nav_menu('main-menu',__( 'Menu' ));
}


// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'html5blank'),
        'description' => __('Description for this widget-area...', 'html5blank'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
//add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
//add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'summit_main_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_staff'); // Add our Staff Custom Post Type
add_action('init', 'create_post_type_testimonials'); // Add our Testimonials Custom Post Type
add_action('init', 'create_post_type_faqs'); // Add our FAQs Custom Post Type
add_action('init', 'create_post_type_notifications'); // Add our Notifications Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
//add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create Staff Custom Post type
function create_post_type_staff()
{
    register_post_type('staff', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Staff', 'staff'), // Rename these to suit
            'singular_name' => __('Staff', 'staff'),
            'add_new' => __('Add Person', 'staff'),
            'add_new_item' => __('Add New Person', 'staff'),
            'edit' => __('Edit', 'staff'),
            'edit_item' => __('Edit Person', 'staff'),
            'new_item' => __('New Person', 'staff'),
            'view' => __('View Staff', 'staff'),
            'view_item' => __('View Person', 'staff'),
            'search_items' => __('Search Staff', 'staff'),
            'not_found' => __('No staff members found', 'staff'),
            'not_found_in_trash' => __('No staff members found in Trash', 'staff')
        ),
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 21,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'capability_type' => 'post'
    ));
}




// Create Testimonials Custom Post type
function create_post_type_testimonials()
{
    register_post_type('testimonials', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Testimonials', 'testimonials'), // Rename these to suit
            'singular_name' => __('Testimonial', 'testimonials'),
            'add_new' => __('Add Testimonial', 'testimonials'),
            'add_new_item' => __('Add New Testimonial', 'testimonials'),
            'edit' => __('Edit', 'testimonials'),
            'edit_item' => __('Edit Testimonial', 'testimonials'),
            'new_item' => __('New Testimonial', 'testimonials'),
            'view' => __('View Testimonials', 'testimonials'),
            'view_item' => __('View Testimonial', 'testimonials'),
            'search_items' => __('Search Testimonials', 'testimonials'),
            'not_found' => __('No testimonials found', 'testimonials'),
            'not_found_in_trash' => __('No testimonials found in Trash', 'testimonials')
        ),
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 23,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'capability_type' => 'post'
    ));
}



// Create FAQs Custom Post type
function create_post_type_faqs()
{
    register_post_type('faqs', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('FAQs', 'faqs'), // Rename these to suit
            'singular_name' => __('FAQ', 'faqs'),
            'add_new' => __('Add FAQ', 'faqs'),
            'add_new_item' => __('Add New FAQ', 'faqs'),
            'edit' => __('Edit', 'faqs'),
            'edit_item' => __('Edit FAQ', 'faqs'),
            'new_item' => __('New FAQ', 'faqs'),
            'view' => __('View FAQs', 'faqs'),
            'view_item' => __('View FAQ', 'faqs'),
            'search_items' => __('Search FAQs', 'faqs'),
            'not_found' => __('No FAQs found', 'faqs'),
            'not_found_in_trash' => __('No FAQs found in Trash', 'faqs')
        ),
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 22,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'capability_type' => 'post'
    ));
}



// Create Notifications Custom Post type
function create_post_type_notifications()
{
    register_post_type('notifications', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Notifications', 'notifications'), // Rename these to suit
            'singular_name' => __('Notification', 'notifications'),
            'add_new' => __('Add Notificaion', 'notifications'),
            'add_new_item' => __('Add New Notification', 'notifications'),
            'edit' => __('Edit', 'notifications'),
            'edit_item' => __('Edit Notification', 'notifications'),
            'new_item' => __('New Notification', 'notifications'),
            'view' => __('View Notifications', 'notifications'),
            'view_item' => __('View Notification', 'notifications'),
            'search_items' => __('Search Notifications', 'notifications'),
            'not_found' => __('No Notifications found', 'notifications'),
            'not_found_in_trash' => __('No Notifications found in Trash', 'notifications')
        ),
        'hierarchical' => false,
        'supports' => array( 'title', 'revisions' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false,
        'capability_type' => 'post'
    ));
}




// =========================================================================
// CREATE TEAMS TAXONOMY
// =========================================================================
//
// function create_department_taxonomy() {
//
// 	$labels = array(
// 		'name'                       => _x( 'Departments', 'taxonomy general name' ),
// 		'singular_name'              => _x( 'Department', 'taxonomy singular name' ),
// 		'search_items'               => __( 'Search Departments' ),
// 		'popular_items'              => __( 'Popular Departments' ),
// 		'all_items'                  => __( 'All Departments' ),
// 		'parent_item'                => null,
// 		'parent_item_colon'          => null,
// 		'edit_item'                  => __( 'Edit Department' ),
// 		'update_item'                => __( 'Update Department' ),
// 		'add_new_item'               => __( 'Add Department' ),
// 		'new_item_name'              => __( 'New Department Name' ),
// 		'separate_items_with_commas' => __( 'Separate departments with commas' ),
// 		'add_or_remove_items'        => __( 'Add or remove department' ),
// 		'choose_from_most_used'      => __( 'Choose from the most used departments' ),
// 		'not_found'                  => __( 'No departments found.' ),
// 		'menu_name'                  => __( 'Departments' ),
// 	);
//
// 	$args = array(
// 		'hierarchical'          => true,
// 		'labels'                => $labels,
// 		'public'                => true,
// 		'show_ui'               => true,
// 		'show_admin_column'     => true,
// 		'update_count_callback' => '_update_post_term_count',
// 		'query_var'             => true,
// 		'rewrite'               => false,
// 	);
//
// 	register_taxonomy( 'departments', 'staff', $args );
// }
// add_action( 'init', 'create_department_taxonomy', 0 );
//





/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}





// =========================================================================
// ADD MINIMAL WYSIWYG TOOLBARS
// =========================================================================


add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars )
{
	// Add a new toolbar called "Very Simple"
	// - this toolbar has only 1 row of buttons
	$toolbars['Very Simple' ] = array();
	$toolbars['Very Simple' ][1] = array('bold' , 'italic' , 'underline', 'link', 'unlink', 'removeformat', 'pastetext' );


	// return $toolbars - IMPORTANT!
	return $toolbars;
}




// =========================================================================
// Query to get the Staff custom post type
// =========================================================================

function get_staff() {
	$args = array(
		'post_type'    =>	'staff',
		'posts_per_page' => -1
	);
	$get_staff_query = new WP_Query( $args );
	return $get_staff_query;
}


// =========================================================================
// Query to get the Testimonials custom post type
// =========================================================================

function get_testimonials() {
	$args = array (
		'post_type'    =>	'testimonials',
		'posts_per_page' => -1
	);
	$get_testimonials_query = new WP_Query( $args );
	return $get_testimonials_query;
}



// =========================================================================
// Change the title placeholder text for the Custom Post Types
// =========================================================================

function change_testimonial_text( $title ){
     $screen = get_current_screen();

     if  ( 'testimonials' == $screen->post_type ) {
          $title = "Enter author's name";
     }

     return $title;
}
add_filter( 'enter_title_here', 'change_testimonial_text' );


function change_staff_text( $title ){
     $screen = get_current_screen();

     if  ( 'staff' == $screen->post_type ) {
          $title = "Enter person's name";
     }

     return $title;
}
add_filter( 'enter_title_here', 'change_staff_text' );



function change_faqs_text( $title ){
     $screen = get_current_screen();

     if  ( 'faqs' == $screen->post_type ) {
          $title = "Enter question";
     }

     return $title;
}
add_filter( 'enter_title_here', 'change_faqs_text' );



function change_notifications_text( $title ){
     $screen = get_current_screen();

     if  ( 'notifications' == $screen->post_type ) {
          $title = "Enter notification title";
     }

     return $title;
}
add_filter( 'enter_title_here', 'change_notifications_text' );





// =========================================================================
// Add Settings Page to Advanced Custom Post Types
// =========================================================================


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Summit Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'      => '24.4',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Forms & Docs',
		'menu_title'	=> 'Forms & Docs',
		'parent_slug'	=> 'theme-general-settings',
	));
}



// =========================================================================
// Hide Unneeded Pages for Non-Admins
// =========================================================================

add_action( 'admin_menu', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
	if( ! current_user_can('administrator') ) {
		remove_menu_page('tools.php');
			remove_menu_page('edit.php');
			remove_menu_page('edit-comments.php');
			//remove_menu_page('upload.php');
			remove_meta_box('pageparentdiv', 'page', 'normal');
	}
}




// =========================================================================
// Remove Query Strings from Static Resources
// =========================================================================
function _remove_script_version( $src ){
$parts = explode( '?ver', $src );
return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );



// =========================================================================
// Image Quality
// =========================================================================
add_filter( 'jpeg_quality', create_function( '', 'return 70;' ) );
add_filter( 'acf-image-crop/image-quality', function( $level ) {
  return 80;
});




// =========================================================================
// Dynamically Populate School Year Select in the Theme Settings
// =========================================================================

function my_acf_load_field( $field ) {
	$thisYear = date("Y") ;

	$years = array();
	for ($i = 0; $i < 3; ++$i) {
		$years[] = $thisYear . "-" . ($thisYear + 1);
		++$thisYear;
	}
	$yearRange = array_combine($years, $years);
	$field['choices'] = $yearRange;
	return $field;
}

add_filter('acf/load_field/name=school-year', 'my_acf_load_field');




?>