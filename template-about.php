<?php /* Template Name: About Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title"><em>Welcome</em> to Summit Christian School</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Thank you for your interest in Summit Christian School! SCS is an interdenominational Christian school partnering with families that are seeking a Preschool through 8th grade education in a close-knit, caring Christian environment.</p>
					<p>Summit focuses on discipleship education. A strong academic curriculum combines with programs that are designed to encourage students to serve the Lord and impact their world through applying their unique gifts, talents, character and leadership abilities in many learning opportunities.</p>
					<p>We invite you to contact us or – even better – <a href="<?php echo get_permalink(294); ?>">visit our campus</a>. Summit has an atmosphere that has to be experienced to be believed. Our hope is that you will join with us at a place where students truly learn and live with Christian character. God has done amazing things at Summit and we'd love for you to be part of it!</p>
					<p>In His service,</p>
					<p><img class="signature" src="<?php echo get_template_directory_uri(); ?>/img/signature.png" alt="image of the Principal's signature" /></p>
					<p><strong>David Couchman, Administrator/Principal</strong></p>
				</div>
			</div>


            <figure class="row">
				<div class="imageGrid col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0 ">
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/about-1.jpg" title="About photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-1.jpg" class="lazy" alt="About photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-1.jpg" class="lazy visible-xs" alt="About photo 1">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/about-2.jpg" title="About photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-2.jpg" class="lazy" alt="About photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-2.jpg" class="lazy visible-xs" alt="About photo 2">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/about-3.jpg" title="About photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-3.jpg" class="lazy" alt="About photo 3">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-3.jpg" class="lazy visible-xs" alt="About photo 3">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/about-4.jpg" title="About photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-4.jpg" class="lazy" alt="About photo 4">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/about-4.jpg" class="lazy visible-xs" alt="About photo 2">
						</div>
					</div>
				</div>
			</figure>



			<section class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					<h3>Our core beliefs & philosophical foundations</h3>
					<h4>Vision</h4>
					<p>Our vision is to equip students to serve the Lord and become leaders who desire to impact their world by their commitment to learn and live with Christian character.</p>

					<h4>Mision</h4>
					<ul>
						<li>To challenge young men and women to grow closer to the Lord Jesus Christ through discipleship.</li>
						<li>To increase their compassion for those around them as well as around the world.</li>
						<li>To advance in knowledge and in skill so that they become leaders who serve the Lord and impact the world through their unique gifts and talents, as well as by their character and leadership.</li>
					</ul>

					<h4>Core Values</h4>
					<ul>
						<li>To partner with parents in advancing knowledge and skill while challenging young men and women to grow closer to our Lord Jesus Christ through discipleship.</li>
						<li>To equip students to grow spiritually, academically, socially, physically and emotionally.</li>
						<li>To develop young Christian leaders with a passion to serve in their churches, our community, and the world.</li>
						<li>To maintain an environment committed to the advancement of academic excellence and opportunities.</li>
					</ul>


					<h4>Statement of Faith</h4>
					<ul>
						<li><strong>WE BELIEVE</strong> the Bible is God’s written revelation to man. It is composed of sixty-six books which are divinely inspired and inerrant in all their parts. The Bible is infallible and the final authoritative source for faith and conduct (Psalm 12:6; 2 Timothy 3:16; 2 Peter 1:21).</li>
						<li><strong>WE BELIEVE</strong> there is one true and living God, eternally existent in three persons – Father, Son, and Holy Spirit, coequal, perfect in their attributes, each deserving worship and obedience. God is the Creator and sustainer of all things which He miraculously accomplished by the Word of His power in six literal days (Genesis 1-2; Exodus 31:17; Deuteronomy 6:4; Isaiah 44:24; 48:16; John 1:3; 2 Corinthians 3:18; 13:14; Colossians 1:15; Hebrews 1:2).</li>
						<li><strong>WE BELIEVE</strong> Jesus Christ is the second person of the Trinity who possesses all the divine attributes.  He is fully God and fully man.  He was conceived miraculously through the virgin birth without losing any of His divine essence.  He lived a sinless life out of obedience to the Father.  His incarnation was for the purpose of revealing God, redeeming men, and ruling over God’s kingdom.  He accomplished our redemption through His shed blood on the cross.  Three days later He rose from the dead and later ascended to heaven where He sits at the right hand of God waiting to return in great glory and power (Isaiah 7:14; John 1:14, 10:30; Acts 1:9-11; Philippians 2:5-11; Colossians 2:9; Hebrews 7:25; 1 Peter 1:18-19; Revelation 19:11-16).</li>
						<li><strong>WE BELIEVE</strong> in the Holy Spirit who is a divine person who regenerates sinful men and baptizes all of them into the body of Christ.  He indwells, sanctifies, instructs, transforms, and empowers all believers for service (Romans 8:14-16; 1 Corinthians 2:7-14; 7:19-20; 12:13; 2 Corinthians 3:18; Titus 3:5).</li>
						<li><strong>WE BELIEVE</strong> in the resurrection of both the saved and the lost; they that are saved unto the resurrection of life [Heaven], and they that are lost unto the resurrection of eternal condemnation [the Lake of Fire] (John 5:28-29; Romans 8:11; Revelation 20:13-15).</li>
						<li><strong>WE BELIEVE</strong> in the absolute necessity of regeneration by the Holy Spirit for salvation.  Due to man’s depravity, he is unable to do or achieve anything that might merit salvation.  Man must be justified on the single ground of faith in the shed blood of Jesus Christ.  It is only because of God’s grace and belief in Christ’s atoning work that anyone can be saved from the penalty, power and ultimately the presence of sin (John 3:16-19; Romans 3:23-25; 5:1-11; Ephesians 2:8-9).</li>
						<li><strong>WE BELIEVE</strong> in the spiritual unity of all believers who have been born again through the Lord Jesus Christ and have been placed in the body of Christ, who is the Head of the Church.  We hold to the fact God does miracles and heals bodies immediately, without human giftedness. (Acts 5:12; Romans 8:9; 1 Corinthians 12:13; 2 Corinthians 12:12; Galatians 3:26-29; Colossians 1:18).  For His creatures, God has instituted marriage as a way for men and women to enjoy companionship.  Sexual intimacy is a wonderful gift of God that is only to be expressed between a man and a woman, as defined genetically, within the love and bonds of marriage.  Therefore, WE BELIEVE that any form of sexual intimacy outside of marriage is both immoral and a perversion of God’s gift (Genesis 2:24-25; Leviticus 18:1-30; Proverbs 5:18; 6:32; Romans 1:26-27; 1 Corinthians 6:18; 7:5; 1 Thessalonians 4:3-5; Hebrews 13:4).</li>
					</ul>
				</div>
			</section>








		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
