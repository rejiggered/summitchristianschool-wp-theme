<?php /* Template Name: Support Template */ get_header(); ?>


	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title"><em>Thank you</em> for giving to Summit Christian School!</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Summit is a non-profit, qualified 501(c)(3) charitable organization that is supported by the time, talents and treasure of many people. Many of our student enrichment programs would not be possible without financial support beyond tuition fees. Your tax-deductible donations make learning possible and are greatly appreciated!</p>
				</div>
			</div>


			<section class="row">
				<div class="col-sm-8 col-sm-offset-4 col-md-4 col-md-offset-0">
					<h4 class="well--heading icon"><svg class="icon-gift"><use xlink:href="<?php echo get_template_directory_uri(); ?>/img/icons.svg#icon-gift"></use></svg>Give Online</h4>
					<p>Make a secure online contribution via PayPal with a credit or debit card. Thank you!</p>
					<div class="well col-sm-7 col-sm-offset-0 col-md-12">
						<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
							<input type="hidden" name="cmd" value="_s-xclick">
							<input type="hidden" name="hosted_button_id" value="NZWGN928GBG3J">
							<button type="submit" class="btn btn-default btn-block">Make a Gift to Summit</button>
						</form>
					</div>
				</div>


				<div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-1">
					<h4>Give by Mail</h4>
					<p>Checks payable Summit Christian School can be mailed to the school office at:</p>
					<address>
						<span><strong><?php the_field('school-street-address', 'option'); ?></strong></span>
						<span><strong><?php the_field('school-city-state-zip', 'option'); ?></strong></span>
					</address>

					<h4>Other Gifts</h4>
					<p>If you would like to make a donation of stock or other personal property, please contact the office at <strong><?php the_field('school-phone', 'option'); ?></strong>.</p>

					<h3>Other Ways to Support</h3>
					<p>Do you shop on Amazon or at Target or Raley’s? Select Summit Christian School as your school of choice, and a portion of your purchases will benefit the school.</p>

					<h4>Fall Fundraiser</h4>
					<p>Each year the school sponsors a fundraiser in the fall. The specific event is announced after the start of the school year. Last year for our Read 2 Rock fundraiser, our students read over 44,000 minutes to raise almost $17,000 for enrichment programs. Check back for more information about this year’s fundraiser.</p>

					<h4>Spring Fundraiser</h4>
					<p>Join us for our most anticipated event of the year – our Annual Dinner Auction in March. With a fast-paced silent auction, an exciting live auction with auctioneer, fantastic music, delicious food, fabulous friends and a fantastic opportunity to support Summit Christian School – it is a night you do not want to miss!</p>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
