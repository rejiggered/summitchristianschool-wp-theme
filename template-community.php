<?php /* Template Name: Our Community Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">We partner <em>with</em> parents to educate your child</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>We believe that parents and guardians should be involved in every aspect of their child's education. We utilize various online tools to help you stay connected. <a href="<?php echo get_permalink(294); ?>">Contact the office</a> for instructions on how to set up new accounts, or if you have questions or need help.</p>
				</div>
			</div>


            <figure class="row">
				<div class="imageGrid col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0 ">
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/community-1.jpg" title="Community photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-1.jpg" class="lazy" alt="Community photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-1.jpg" class="lazy visible-xs" alt="Community photo 1">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/community-2.jpg" title="Community photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-2.jpg" class="lazy" alt="Community photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-2.jpg" class="lazy visible-xs" alt="Community photo 2">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/community-3.jpg" title="Community photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-3.jpg" class="lazy" alt="Community photo 3">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-3.jpg" class="lazy visible-xs" alt="Community photo 3">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/community-4.jpg" title="Community photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-4.jpg" class="lazy" alt="Community photo 4">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/community-4.jpg" class="lazy visible-xs" alt="Community photo 2">
						</div>
					</div>
				</div>
			</figure>




			<section class="row">
				<a name="tools"></a>
				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					<h3>Stay Connected with Us</h3>
					<div class="row">

						<div class="col-sm-6">
							<h4>RENWEB</h4>
							<p>Through RENWEB, our chosen school management software, you will have access to your child's grades, test scores, daily homework assignments and notes from the teacher.</p>
							<p><a href="https://www.renweb.com/Logins/ParentsWeb-Login.aspx" class="btn btn-default">Go to RENWEB</a></p>
						</div>
						<div class="col-sm-6">
							<h4>FACTS</h4>
							<p>Parents can manage tuition payments and billings for Extended Care through FACTS Management software.</p>
							<p><a href="https://online.factsmgt.com/signin/3CRTT" class="btn btn-default">Go to FACTS</a></p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<h4>School Calendar</h4>
							<p>Planning ahead? Download the current school year calendar for reference.</p>
							<p><a href="<?php $file = get_field('school-calendar', 'option'); echo $file['url']; ?>" class="btn btn-default">School Calendar</a></p>
						</div>
						<div class="col-sm-6">
							<h4>Facebook</h4>
							<p>Join us on Facebook to see the latest news and happening.</p>
							<p><a href="https://www.facebook.com/summitcs" class="btn btn-default">Facebook</a></p>
						</div>
					</div>
				</div>
			</section>





		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
