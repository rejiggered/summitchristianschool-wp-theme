<?php /* Template Name: Academics - Elementary Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">We support the academic, <em>spiritual</em> and social development of your child</h2>
				<div class="lead col-sm-8 col-md-7">
					<p>Summit Christian School is a warm, caring, and loving Christian community, offering a school environment that supports the academic, spiritual and social development of your child. Our small class sizes allow for building mentoring relationships between teachers and students. We are family-oriented and support the development of positive peer relationships among students.</p>
					<p>Summit strives to cultivate knowledge, application, and relationships so that good fruit may be harvested. Our goal is to encourage students to become positive problem-solvers. Our delight is training up students to be a light in the world and sending them out to be an extension of the hands and feet of Jesus.</p>
				</div>
			</div>


            <figure class="row">
				<div class="imageGrid col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0 ">
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-1.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-1.jpg" class="lazy" alt="Middle School photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-1.jpg" class="lazy visible-xs" alt="Middle School photo 1">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-2.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-2.jpg" class="lazy" alt="Middle School photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-2.jpg" class="lazy visible-xs" alt="Middle School photo 2">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-3.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-3.jpg" class="lazy" alt="Middle School photo 3">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-3.jpg" class="lazy visible-xs" alt="Middle School photo 3">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-4.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-4.jpg" class="lazy" alt="Middle School photo 4">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-4.jpg" class="lazy visible-xs" alt="Middle School photo 2">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-5.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-5.jpg" class="lazy" alt="Middle School photo 5">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-5.jpg" class="lazy visible-xs" alt="Middle School photo 5">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-6.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-6.jpg" class="lazy" alt="Middle School photo 6">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-6.jpg" class="lazy visible-xs" alt="Middle School photo 6">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-7.jpg" title="Middle School photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-7.jpg" class="lazy" alt="Middle School photo 7">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/middleschool-7.jpg" class="lazy visible-xs" alt="Middle School photo 7">
						</div>
					</div>
				</div>
			</figure>


			<section class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					<h3>Curriculum</h3>
					<p>Besides the standard classroom subjects of Math, Language Arts, History, Science, Bible, etc., our curriculum includes:</p>
					<ul>
						<li>Chapel</li>
						<li>Physical education</li>
						<li>Art</li>
						<li>Music and performing arts</li>
						<li>Fields trips</li>
						<li>Band</li>
					</ul>

					<h3>Extra-curricular Activities</h3>
					<p>In addition to our academic curriculum, the development of our students includes many extra-curricular activities:</p>
					<ul>
						<li>Sports program:
							<ul>
								<li>Basketball</li>
								<li>Flag Football</li>
								<li>Soccer</li>
								<li>Golf</li>
								<li>Dance</li>
							</ul>
						</li>
						<li>Science camp</li>
						<li>Washington, D.C. trip</li>
						<li>Student council</li>
					</ul>
					<p>At our weekly chapel services, students participate and worship through music and hear a message from visiting pastors and speakers.</p>
					<p>Our weekly music program introduces our students to all aspects of performance including vocals, drama, set design, sound and lighting technical support. Two highlights of our school year are when our students share special performances during the Christmas and Spring concerts.</p>


					<?php get_template_part( 'includes/content', 'calendar' ); ?>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
