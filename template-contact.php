<?php /* Template Name: Contact Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">We would love to hear from <em>you</em></h2>
				<div class="lead col-sm-8 col-md-7">
					<p>For more information about our school and how you can become a member of our community, please stop by our office. Or call us to schedule a tour, today! We offer “shadow days” for your child to experience a day in the life of a Summit student.</p>
				</div>

				<div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-5">
					<div class="row">
						<div class="col-sm-6 contactInfo">
							<h4>Address</h4>
							<address>
								<span><?php the_field('school-street-address', 'option'); ?></span>
								<span><?php the_field('school-city-state-zip', 'option'); ?></span>
								<span class="margin-top10"><strong>Phone:</strong> <?php the_field('school-phone', 'option'); ?></span>
								<span><strong>Fax:</strong> <?php the_field('school-fax', 'option'); ?></span>
								<span><strong>Email:</strong> <a href="mailto:<?php the_field('school-email', 'option'); ?>"><?php the_field('school-email', 'option'); ?></a></span>
							</address>
							<a href="https://goo.gl/maps/F9dVgmpsouu" class="btn btn-default">Get Directions</a>
						</div>
						<div class="col-sm-6">
							<h4>Office Hours</h4>
							<p><strong><span class="block">(School Year)</span></strong>M – F, <?php the_field('officeOpen-school', 'option'); ?> - <?php the_field('officeClose-school', 'option'); ?></p>
							<p><strong><span class="block">(Summer)</span></strong>M – W, <?php the_field('officeOpen-summer', 'option'); ?> – <?php the_field('officeClose-summer', 'option'); ?></p>
							<h4 class="margin-top30">Follow</h4>
							<p><a href="https://www.facebook.com/summitcs">Facebook</a></p>
						</div>
					</div>
				</div>
			</div>


			<section class="row map">
				<div class="col-xs-12 map--inner">
					<iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d49852.51969365676!2d-121.21932545017471!3d38.65388158042758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x809ae72f0a851977%3A0x1632596927396e76!2sSummit+Christian+School+Fair+Oaks%2C+CA+95628!3m2!1d38.656836!2d-121.2250758!5e0!3m2!1sen!2sus!4v1490639733489" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</section>


			<section class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					<h3>Kindergarten Information</h3>
					<p>Join us for our Kindergarten information night held in February each year. Find out why we offer a full-day Kindergarten program, check out the classroom, look through the curriculum, and meet our teacher.</p>
					<h3>Online Tools</h3>
					<p>We believe that parents and guardians should be involved in every aspect of their child's education. Through <strong>RENWEB</strong>, our chosen school software, you will have access to your <a href="https://www.renweb.com/Logins/ParentsWeb-Login.aspx">child's grades, test scores, daily homework assignments and notes</a> from the teacher.</p>
					<p><a href="https://www.renweb.com/Logins/ParentsWeb-Login.aspx" class="btn btn-default">Go to RENWEB</a></p>
					<p class="margin-top30">Parents can also <a href="https://online.factsmgt.com/signin/3CRTT">manage their tuition payments and invoices</a> for Extended Care through <strong>FACTS</strong>.</p>
					<p><a href="https://online.factsmgt.com/signin/3CRTT" class="btn btn-default">Go to FACTS</a></p>

					<?php get_template_part( 'includes/content', 'calendar' ); ?>
				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
