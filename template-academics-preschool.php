<?php /* Template Name: Academics - Preschool Template */ get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<div class="row pageIntro">
				<h2 class="col-sm-4 col-md-5 pageIntro--title">We offer developmentally <em>age-appropriate</em> programs</h2>
				<div class="lead col-sm-8 col-md-7">
					<?php the_field('page-intro'); ?>
				</div>
			</div>


			<figure class="row">
				<div class="imageGrid col-sm-12 col-sm-offset-0 col-md-12 col-md-offset-0 ">
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-1.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-1.jpg" class="lazy" alt="Preschool photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-1.jpg" class="lazy visible-xs" alt="Preschool photo 1">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-2.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-2.jpg" class="lazy" alt="Preschool photo 1">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-2.jpg" class="lazy visible-xs" alt="Preschool photo 2">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-3.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-3.jpg" class="lazy" alt="Preschool photo 3">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-3.jpg" class="lazy visible-xs" alt="Preschool photo 3">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-4.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-4.jpg" class="lazy" alt="Preschool photo 4">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-4.jpg" class="lazy visible-xs" alt="Preschool photo 2">
						</div>
					</div>
					<div class="image-container third">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-5.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-5.jpg" class="lazy" alt="Preschool photo 5">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-5.jpg" class="lazy visible-xs" alt="Preschool photo 5">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-6.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-6.jpg" class="lazy" alt="Preschool photo 6">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-6.jpg" class="lazy visible-xs" alt="Preschool photo 6">
						</div>
					</div>
					<div class="image-container">
						<div class="image">
							<a class="gallery hidden-xs" data-vbgall="gallery" href="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-7.jpg" title="Preschool photos">
								<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-7.jpg" class="lazy" alt="Preschool photo 7">
							</a>
							<img data-src="<?php echo get_template_directory_uri(); ?>/img/photos/preschool-7.jpg" class="lazy visible-xs" alt="Preschool photo 7">
						</div>
					</div>
				</div>
			</figure>


			<section class="row">
				<div class="col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
					<h3>Curriculum</h3>
					<p>Each preschool class has been carefully designed to foster development of the whole child. Bible instruction and application of its principals are integral parts of all instruction in the classroom, which creates a heart for God and learning. We use <a href="http://www.bjupress.com/">Bob Jones Curriculum</a> for Threes and Fours as a precursor to the Bob Jones Curriculum used for grades K-8 here at Summit, with an additional Bible curriculum chosen to suit the developmental level of each preschool class. Included in the curriculum are:</p>
					<ul>
						<li>Pre-math skills (number recognition, one-to-one correspondence)</li>
						<li>Pre-reading (phonemic awareness, upper and lower case letter recognition)</li>
						<li>Critical thinking and listening skills</li>
						<li>Music and movement</li>
						<li>Art</li>
						<li>Imaginative play</li>
						<li>Field trips</li>
						<li>Extra-curricular activites (provided on site by outside vendors at an additional cost)</li>
					</ul>
					<p>Within the classroom community your child will be encouraged to become both self-sufficient and develop an awareness of others. Daily, children practice working together, demonstrating obedience and responsibility for their surroundings thorugh routines and classroom jobs. We emphasize verbal communication with teachers and peers to express ideas and feelings. In the event of a conflict between children, we teach effective problem solving through acknowledgement of feelings, repentance and prayer. These skills will promote confidence and a sense of what is right which will equip children for a successful future.</p>

					<p class="small"><em>License #343616738</em></p>

					<?php get_template_part( 'includes/content', 'calendar' ); ?>

				</div>
			</section>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
